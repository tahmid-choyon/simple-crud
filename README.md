# A RESTful api and Simple Crud operations

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/112e543830e6414abe21809859da2362)](https://app.codacy.com/app/tahmid-choyon/simple-crud?utm_source=github.com&utm_medium=referral&utm_content=tahmid-choyon/simple-crud&utm_campaign=Badge_Grade_Dashboard)

A very simple rest api using simpler crud operations.

- Language: **Python 3.6**
- Framework: **Flask**
- Database: **Postgres**
- ORM: **SQLAlchemy**

### How to run
```bash
pip install -r requirements.txt
python app.py
```
#### Browse http://127.0.0.1:5555/