Endpoint                           Methods           Rule
---------------------------------  ----------------  ------------------------------------------------------
home                               GET               /
product_blueprint.productid        DELETE, GET, PUT  /api/v1/product/products/<string:product_uid>
product_blueprint.productlist      GET, POST         /api/v1/product/products
product_blueprint.purchaseproduct  GET               /api/v1/product/products/<string:product_uid>/purchase
static                             GET               /static/<path:filename>
user_blueprint.login               POST              /api/v1/user/login
user_blueprint.profile             DELETE, GET, PUT  /api/v1/user/me
user_blueprint.signup              POST              /api/v1/user/signup
user_blueprint.userattributes      GET               /api/v1/user/me/attribute/<string:attribute_name>
