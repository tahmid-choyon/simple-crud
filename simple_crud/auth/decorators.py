from functools import wraps

from flask import request, make_response, jsonify

from simple_crud.database.models.database_models import User
from simple_crud.utilities.jwt_token_utilities import *


def extract_token_from_request():
    return request.headers['Authorization'].split(' ')[1]


def generate_error_response(msg: str = "Error", code=403):
    return make_response(jsonify({
        "msg": msg
    }), code)


def token_required(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        if 'Authorization' not in request.headers:
            return generate_error_response(msg="Authorization header in not present", code=401)
        bearer_token = request.headers.get('Authorization')
        if not bearer_token:
            return generate_error_response(msg="Bearer Token is not present", code=401)
        try:
            token = bearer_token.split(' ')[1]
        except:
            return generate_error_response(msg="Invalid Bearer Token format", code=400)
        decoded_token, err = decode_token(token)
        if err:
            return generate_error_response(msg=decoded_token, code=400)
        return func(*args, **kwargs)

    return wrap


def json_data_required(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        if not request.is_json:
            return generate_error_response(msg="JSON data is expected but not found", code=400)
        return func(*args, **kwargs)

    return wrap


def valid_user_token_required(func):
    @wraps(func)
    @token_required
    def wrap(*args, **kwargs):
        token = extract_token_from_request()
        decoded, err = decode_token(token)
        user = User.query.get(decoded.get('uid'))
        if not user:
            return generate_error_response(msg="Invalid token", code=401)
        kwargs['user_uid'] = decoded['uid']
        return func(*args, **kwargs)

    return wrap
