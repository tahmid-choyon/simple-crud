from simple_crud.database.models.database_models import *


def get_product_from_uid(uid: str) -> Product:
    return Product.query.get(uid)


def get_all_products(page=1, limit=5):
    return [product.to_json() for product in Product.query.order_by(
        Product.posted_at.desc()
    ).paginate(page, limit, False).items], Product.query.count()


def create_product(user: User, **kwargs) -> Product:
    product = Product(**kwargs)
    user.products.append(product)
    db.session.commit()
    return product


def delete_product(user_uid, product_uid):
    product = Product.query.filter(
        (Product.user_uid == user_uid) &
        (Product.uid == product_uid)
    ).first()
    if product:
        db.session.delete(product)
        db.session.commit()
        return 204
    return 404


def purchase_product(user: User, product_uid: str) -> list:
    product = get_product_from_uid(product_uid)
    user.purchases.append(product)
    db.session.commit()

    return user.get_purchases()


if __name__ == '__main__':
    print(get_product_from_uid('61a5505d-7bba-4997-8f18-0d79ac186a5b'))
