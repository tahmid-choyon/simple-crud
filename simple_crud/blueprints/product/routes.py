import math

from flask import Blueprint, request, make_response, jsonify
from flask_restful import Api, Resource

from simple_crud.auth.decorators import json_data_required, generate_error_response, \
    valid_user_token_required
from simple_crud.blueprints.user.user_crud_ops import get_user_from_uid
from .product_crud_ops import *

productbp = Blueprint("product_blueprint", __name__)
api = Api(productbp, prefix="/product")


def valid_product_uid_required(func):
    def wrap(*args, **kwargs):
        product_uid = kwargs.get('product_uid')
        product = get_product_from_uid(product_uid)
        if not product:
            return generate_error_response(msg="Product not found", code=404)
        return func(*args, **kwargs)

    return wrap


class ProductList(Resource):
    method_decorators = [valid_user_token_required]

    def get(self, *args, **kwargs):
        page, limit = request.args.get('page', default=1, type=int), request.args.get('limit', default=5, type=int)
        products, rows = get_all_products(page, limit)
        return make_response(jsonify({
            "total_page": math.ceil(rows / limit),
            "page": page,
            "limit": limit,
            "results": products
        }), 200)

    @json_data_required
    def post(self, *args, **kwargs):
        user = get_user_from_uid(kwargs.get('user_uid'))
        data = request.get_json()
        product = create_product(user, **data)
        return make_response(jsonify(
            product.to_json()
        ), 201)


api.add_resource(ProductList, "/products")


class ProductID(Resource):
    method_decorators = [valid_user_token_required, valid_product_uid_required]

    def get(self, product_uid=None, *args, **kwargs):
        product = get_product_from_uid(product_uid)
        return make_response(jsonify(
            product.to_json()
        ), 200)

    def put(self, product_uid=None, *args, **kwargs):
        return make_response(jsonify({
            "msg": "Operation is not supported right now"
        }), 200)

    def delete(self, product_uid=None, *args, **kwargs):
        product = delete_product(user_uid=kwargs.get('user_uid'), product_uid=product_uid)
        if product == 404:
            return generate_error_response(msg="You do not have permission for attempted request", code=403)
        return "", 204


api.add_resource(ProductID, "/products/<string:product_uid>")


class PurchaseProduct(Resource):
    method_decorators = [valid_user_token_required, valid_product_uid_required]

    def get(self, product_uid=None, *args, **kwargs):
        user = get_user_from_uid(kwargs.get('user_uid'))
        product_list = purchase_product(user, product_uid)
        return make_response(jsonify(
            product_list
        ), 200)


api.add_resource(PurchaseProduct, "/products/<string:product_uid>/purchase")
