from flask import Blueprint, request, make_response, jsonify, redirect, url_for
from flask_restful import Api, Resource

from simple_crud.auth.decorators import json_data_required, generate_error_response, \
    valid_user_token_required
from .user_crud_ops import *

userbp = Blueprint("user_blueprint", __name__)
api = Api(userbp, prefix="/user")


def valid_attribute_name(func):
    def wrap(*args, **kwargs):
        if not hasattr(User, f"get_{kwargs.get('attribute_name')}"):
            return generate_error_response(msg=f"Invalid attribute {kwargs.get('attribute_name')}", code=404)
        return func(*args, **kwargs)

    return wrap


class Signup(Resource):
    method_decorators = [json_data_required]

    def post(self):
        data = request.get_json()
        username = data.pop('username', None)
        email = data.pop('email', None)
        password = data.pop('password', None)
        if any(item is None for item in [username, password, email]):
            return generate_error_response(msg="username, password, email field is mandatory", code=403)

        user = create_new_user(username, email, password, **data)
        if isinstance(user, str):
            return generate_error_response(msg=user, code=403)

        return make_response(jsonify(
            user.to_json()
        ), 201)


api.add_resource(Signup, "/signup")


class Login(Resource):
    method_decorators = [json_data_required]

    def post(self):
        data = request.get_json()
        username = data.get('username')
        password = data.get('password')
        if any(item is None for item in [username, password]):
            return generate_error_response(msg="username, password field is mandatory", code=403)

        if not check_user_credentials(username, password):
            return generate_error_response(msg="Invalid username/password combination", code=403)

        user = get_user_from_username(username)
        token = generate_token_for_user(user.uid)
        return make_response(jsonify(
            {
                "token": token
            }
        ), 200)


api.add_resource(Login, "/login")


class Profile(Resource):
    method_decorators = [valid_user_token_required]

    def get(self, *args, **kwargs):
        user = get_user_from_uid(kwargs.get('user_uid'))
        return make_response(jsonify(
            user.to_json()
        ), 200)

    @json_data_required
    def put(self, *args, **kwargs):
        user = get_user_from_uid(kwargs.get('user_uid'))
        data = request.get_json()
        user, err = update_user(uid=user.uid, **data)
        if err:
            return generate_error_response(msg=user, code=500)
        return redirect(url_for("user_blueprint.profile", user_uid=user.uid))

    def delete(self, *args, **kwargs):
        return make_response(jsonify({
            "msg": "Operation is not supported right now"
        }), 200)


api.add_resource(Profile, "/me")


class UserAttributes(Resource):
    method_decorators = [valid_user_token_required, valid_attribute_name]

    def get(self, attribute_name=None, *args, **kwargs):
        user = get_user_from_uid(kwargs.get('user_uid'))
        attrib_data = getattr(user, f"get_{attribute_name}")()
        return make_response(jsonify(
            attrib_data
        ), 200)


api.add_resource(UserAttributes, "/me/attribute/<string:attribute_name>")
