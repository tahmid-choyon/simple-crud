import logging
from typing import Union

from simple_crud.database.models.database_models import *

logger = logging.getLogger(__name__)


def drop():
    logger.warning("Dropping database...")
    db.drop_all()


def create():
    logger.warning("Creating database schema...")
    db.create_all()


def seed_user() -> Union[User, list]:
    logger.warning("Seeding Users...")
    user = User(username="tahmid", email="tahmid@gmail.com", password="123456")
    user.add_address(road_no="12", area="Banani", city="Dhaka", zip_code="1213", country="BD")

    user_two = User(username="buyer", email="buyer@gmail.com", password="123456")
    user_two.add_address(road_no="12", area="Uttara", city="Dhaka", zip_code="1213", country="BD")

    db.session.add_all([user, user_two])
    db.session.commit()
    return [user, user_two]


def seed_product(user: User) -> Product:
    logger.warning("Seeding Products...")
    product = Product(name="Mouse", description="Computer mouse", price=10.00, images=["image1", "image2"])
    user.products.append(product)
    db.session.commit()
    return product


def add_purchase_history(user: User, product: Product):
    logger.warning("Seeding Purchase history...")
    user.purchases.append(product)
    product.buyers.append(user)
    db.session.commit()


if __name__ == '__main__':
    drop()
    create()
    users = seed_user()
    user = users[0]
    product = seed_product(user)
    add_purchase_history(users[1], product)
    logger.warning("DONE!")
    print("User[0] -> ", user.profile_info())
    print("User[0] Products -> ", user.get_products())
    print("Product -> ", product.to_json())
    print("Product buyers -> ", product.get_buyers())
    print("User[1] purchase -> ", users[1].get_purchases())
